import numpy as np
import struct

def int_to_bytes(value):
    while value < 1:
        value += 0.01

    for value in np.arange(0, 301, 0.1):
        bytes_value = struct.pack('>f', value)
        print(f"in bytes: {bytes_value} | number value: {value}")


def string_to_bytes(string_value):
    bytes_string = string_value.encode('utf-8')
    for byte in bytes_string:
        print(f"string value: {string_value} {byte}")
    decoded_string = bytes_string.decode('utf-8')
    print(f"decoded string: {decoded_string}")
    return bytes_string



def bytes_to_string(bytes_value):
    return bytes_value.decode('utf-8')


def main():
    int_to_bytes(11)
    string_to_bytes("cacao cu lapte")


if __name__ == "__main__":
    main()